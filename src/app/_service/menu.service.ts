import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Menu } from './../_model/menu';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  menuCambio = new Subject<Menu[]>();
  mensajeCambio = new Subject<string>();
  url: string = `${environment.HOST}`;    

  constructor(private http: HttpClient) { }

  listar() {
    let access_token = sessionStorage.getItem(environment.TOKEN_NAME);

    return this.http.get<Menu[]>(`${this.url}/menus`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  listarPorUsuario(nombre: string) {
    let access_token = sessionStorage.getItem(environment.TOKEN_NAME)
    
    return this.http.post<Menu[]>(`${this.url}/menus/usuario`, nombre, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  listarAll(){
    return this.http.get<Menu[]>(`${this.url}/menus`);
  }

  listarPorId(id: number) {
    return this.http.get<Menu>(`${this.url}/menus/${id}`);
  }

  registrar(menu: Menu) {
    return this.http.post(`${this.url}/menus`, menu);
  }

  modificar(menu: Menu) {
    return this.http.put(`${this.url}/menus`, menu);
  }

  eliminar(id: number) {
    return this.http.delete(`${this.url}/menus/${id}`);
  }
}
