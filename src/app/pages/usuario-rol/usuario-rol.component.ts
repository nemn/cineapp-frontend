import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { switchMap } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Usuario } from '../../_model/usuario';
import { UsuarioService } from '../../_service/usuario.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UsuarioAsignarRolComponent } from './usuario-asignar-rol/usuario-asignar-rol.component';

@Component({
  selector: 'app-usuario-rol',
  templateUrl: './usuario-rol.component.html',
  styleUrls: ['./usuario-rol.component.css']
})
export class UsuarioRolComponent implements OnInit {

 
  usuario: Usuario;
  dataSource: MatTableDataSource<Usuario>;
  isusuarioAsignar: boolean = false;
  selectRouteMessage: string = "Asignar rol a usuario";
  displayedColumns = ['idUsuario', 'nombre',  'estado', "asignar"];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private router: Router, private usuarioService: UsuarioService, private dialog: MatDialog, private snackBar: MatSnackBar) {

      console.log(this.router.url);

  }

  ngOnInit(): void {
    this.usuarioService.usuarioCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
    this.usuarioService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });
    this.usuarioService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      console.log(data);
    });
  }

  filter(x: string) {
    this.dataSource.filter = x.trim().toLowerCase();
  }
  eliminar(idusuario: number) {
    this.usuarioService.eliminar(idusuario).pipe(switchMap(() => {
      return this.usuarioService.listar();
    })).subscribe(data => {
      this.usuarioService.usuarioCambio.next(data);
      this.usuarioService.mensajeCambio.next('SE ELIMINO');
    });
  }
  openDialog(usuario: Usuario) {
    let usuarioSelect = usuario != null ? usuario : new Usuario();
    this.dialog.open(UsuarioAsignarRolComponent, {
      width: '600',
      height: '600',
      data: usuarioSelect
    });
  }

}
