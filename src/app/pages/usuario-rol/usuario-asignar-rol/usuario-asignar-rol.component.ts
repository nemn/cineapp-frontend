import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Rol } from '../../../_model/rol';
import { RolService } from '../../../_service/rol.service';
import { UsuarioService } from '../../../_service/usuario.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Usuario } from '../../../_model/usuario';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-usuario-asignar-rol',
  templateUrl: './usuario-asignar-rol.component.html',
  styleUrls: ['./usuario-asignar-rol.component.css']
})
export class UsuarioAsignarRolComponent implements OnInit {


  rols: Rol[];
  rolsSelect: Rol[];
  idusuario: number;
  idRolSelect: number[];
  usuarioSelect: Usuario;
  removable: boolean = true;
  selectable: boolean = true;
  dataSource: MatTableDataSource<Rol>;
  displayedColumns = ['select', 'idRol', 'nombre', 'descripcion'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  selection = new SelectionModel<Rol>(true, []);
  checkedSelection = new SelectionModel<Rol>(true, []);
  checked: { [idRol: number]: boolean; } = {};

  constructor(
    private dialogRef: MatDialogRef<UsuarioAsignarRolComponent>,
    private route: ActivatedRoute,
    private rolService: RolService,
    private usuarioService: UsuarioService,
    @Inject(MAT_DIALOG_DATA) public data: Usuario,
  ) { }

  ngOnInit(): void {

    this.rolsSelect = new Array;
    this.usuarioSelect = new Usuario();
    if (this.data.idUsuario > 0) {
      this.usuarioService.listarPorId(this.data.idUsuario).subscribe(data =>{

      this.usuarioSelect.idUsuario = data.idUsuario;
      this.usuarioSelect.nombre = data.nombre;
      this.usuarioSelect.cliente = data.cliente;
      this.usuarioSelect.clave=data.clave;
      this.usuarioSelect.estado=data.estado;
      this.usuarioSelect.roles = data.roles;
      this.rolsSelect =data.roles;
      })
      this.getListRoles();
      console.log(this.usuarioSelect);

    }
  }
  operar() {
    if (this.usuarioSelect != null && this.usuarioSelect.idUsuario > 0) {
      this.usuarioService.modificar(this.usuarioSelect).subscribe(data => {
        this.usuarioService.listar().subscribe(usuarios => {
          this.usuarioService.usuarioCambio.next(usuarios);
          this.usuarioService.mensajeCambio.next("Se modifico");
        });
      });
    } else {
      this.usuarioService.registrar(this.usuarioSelect).subscribe(data => {
        this.usuarioService.listar().subscribe(usuarios => {
          this.usuarioService.usuarioCambio.next(usuarios);
          this.usuarioService.mensajeCambio.next("Se registro");
        });
      });
    }
    this.dialogRef.close();
  }

  public getListRoles() {
    this.rolService.listar().subscribe(data => {
      this.rols = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.rolsSelect.forEach(rol => {
        this.dataSource.data.forEach(row => {
          if (row.idRol == rol.idRol) {
            this.selection.select(row)
            console.log(row)
          }
        })
        })

      console.log(data);
    })
  }
  addSelectRolToUser() {
    this.rolsSelect = new Array;
    this.selection.selected.forEach(data => {
      console.log(data);
      this.rolsSelect.push(data);
    })
    console.log(this.rolsSelect);
  }

  saveUsuarioAsignRol(){
    this.usuarioSelect.roles=this.rolsSelect;
    this.usuarioService.modificar(this.usuarioSelect).subscribe(data =>{
      this.getListRoles();
      this.usuarioService.mensajeCambio.next("Se Asigno roles al usuario");
      this.dialogRef.close();
    })
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => {
        this.selection.select(row);
        this.rolsSelect.push(row);
      });
    console.log(this.data);
    console.log(this.rolsSelect);
  }

  masterCheckedToggle() {
    this.isAllCheckedSelected() ?
      this.checkedSelection.clear() : this.dataSource.data.forEach(row => {
        this.checkedSelection.select(row);
        this.rolsSelect.push(row);
      });
    console.log(this.rolsSelect);
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  isAllCheckedSelected() {  // confirma si se selecciono todas las filas
    const numSelected = this.checkedSelection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  remove(rol: Rol): void { // deselecciona si se remueve del chip
    const index = this.rolsSelect.indexOf(rol);
    if (index >= 0) {
      this.rolsSelect.splice(index, 1);
      this.dataSource.data.forEach(row => { if (row == rol) this.selection.deselect(row) });
    }
  }

  cancelar() {
    this.dialogRef.close();
  }
}
