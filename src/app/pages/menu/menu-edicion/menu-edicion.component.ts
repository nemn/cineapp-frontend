import { Component, OnInit } from '@angular/core';
import { Menu } from '../../../_model/menu';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { MenuService } from '../../../_service/menu.service';

@Component({
  selector: 'app-menu-edicion',
  templateUrl: './menu-edicion.component.html',
  styleUrls: ['./menu-edicion.component.css']
})
export class MenuEdicionComponent implements OnInit {
  menu: Menu;
  id: number;
  edicion: boolean;
  form: FormGroup;
  idGeneroSeleccionado: number;
  urlImagen: string;
  message:string;
  constructor(
    private route: ActivatedRoute,
    private menuService: MenuService,
    private router: Router) { }
  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = this.id != null;
      console.log(this.id);
      this.initForm();
    });


  }
  initForm() {
    if (this.id != undefined) {
      this.message="Modificar menu";
      this.menuService.listarPorId(this.id).subscribe(data => {
        this.menu=data;
        console.log(this.menu);
        this.form = new FormGroup({
          'idMenu': new FormControl(data.idMenu),
          'nombre': new FormControl(data.nombre),
          'icono': new FormControl(data.icono),
          'url': new FormControl(data.url),
        })
      });
    } else {
      this.message="Nuevo menu";
      this.form = new FormGroup({
        'idMenu': new FormControl(0),
        'nombre': new FormControl('', [Validators.required, Validators.minLength(3)]),
        'icono': new FormControl('', Validators.required),
        'url': new FormControl('', Validators.required),
      });
    }
  }
  get f() { return this.form.controls; }
  operar() {
    console.log(this.form.invalid);
    if (this.form.invalid) {
      return false;
    }
    let menu = new Menu();
    menu.idMenu = this.form.value['idMenu'];
    menu.nombre = this.form.value['nombre'];
    menu.icono = this.form.value['icono'];
    menu.url = this.form.value['url'];
    if(this.menu != undefined)
    menu.roles=this.menu.roles;
    console.log(menu.idMenu);
    if (menu.idMenu > 0) {
      this.menuService.modificar(menu).pipe(switchMap(() => {
        return this.menuService.listar();
      })).subscribe(data => {
        this.menuService.menuCambio.next(data);
        this.menuService.mensajeCambio.next('SE MODIFICO CON EXITO');

      });
    } else {
      this.menuService.registrar(menu).subscribe(() => {
        this.menuService.listar().subscribe(data => {
          this.menuService.menuCambio.next(data);
          this.menuService.mensajeCambio.next('SE REGISTRO CON EXITO');
        });
      })
    }
    this.router.navigate(['menu']);
  }

}
