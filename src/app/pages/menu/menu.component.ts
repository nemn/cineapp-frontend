import { Component, OnInit, ViewChild } from '@angular/core';
import { Menu } from '../../_model/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MenuService } from '../../_service/menu.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MenuAsignarComponent } from './menu-asignar/menu-asignar.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  menu: Menu;
  dataSource: MatTableDataSource<Menu>;
  isMenuAsignar: boolean = false;
  selectRouteMessage: string;
  displayedColumns = [];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private router: Router, private menuService: MenuService, private dialog: MatDialog, private snackBar: MatSnackBar) {
    if (router.url == '/menu-rol') {
      this.isMenuAsignar = true;
      this.displayedColumns = ['idMenu', 'nombre', 'icono', 'img', 'url', "asignar"];
      this.selectRouteMessage="Asignar Rol A Menu";
    } else {
      this.isMenuAsignar = false;
      this.displayedColumns = ['idMenu', 'nombre', 'icono', 'img', 'url', 'acciones'];
      this.selectRouteMessage="Lista de Menu";
    }
      console.log(this.router.url);

  }

  ngOnInit(): void {
    this.menuService.menuCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
    this.menuService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });
    this.menuService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      console.log(data);
    });
  }

  filter(x: string) {
    this.dataSource.filter = x.trim().toLowerCase();
  }
  eliminar(idmenu: number) {
    this.menuService.eliminar(idmenu).pipe(switchMap(() => {
      return this.menuService.listar();
    })).subscribe(data => {
      this.menuService.menuCambio.next(data);
      this.menuService.mensajeCambio.next('SE ELIMINO');
    });
  }
  openDialog(menu: Menu) {
    let menuSelect = menu != null ? menu : new Menu();
    this.dialog.open(MenuAsignarComponent, {
      width: '600',
      height: '600',
      data: menuSelect
    });
  }

}
