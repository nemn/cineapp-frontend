import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Rol } from '../../../_model/rol';
import { RolService } from '../../../_service/rol.service';
import { MenuService } from '../../../_service/menu.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Menu } from '../../../_model/menu';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-menu-asignar',
  templateUrl: './menu-asignar.component.html',
  styleUrls: ['./menu-asignar.component.css']
})
export class MenuAsignarComponent implements OnInit {

  rols: Rol[];
  rolsSelect: Rol[];
  idMenu: number;
  idRolSelect: number[];
  menuSelect: Menu;
  removable: boolean = true;
  selectable: boolean = true;
  dataSource: MatTableDataSource<Rol>;
  displayedColumns = ['select', 'idRol', 'nombre', 'descripcion'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  selection = new SelectionModel<Rol>(true, []);
  checkedSelection = new SelectionModel<Rol>(true, []);

  constructor(
    private dialogRef: MatDialogRef<MenuAsignarComponent>,
    private route: ActivatedRoute,
    private rolService: RolService,
    private menuService: MenuService,
    @Inject(MAT_DIALOG_DATA) public data: Menu,
  ) { }

  ngOnInit(): void {

    this.rolsSelect = new Array;
    this.menuSelect = new Menu();
    if (this.data.idMenu > 0) {
      this.menuService.listarPorId(this.data.idMenu).subscribe(data =>{

      this.menuSelect.idMenu = data.idMenu;
      this.menuSelect.nombre = data.nombre;
      this.menuSelect.url = data.url;
      this.menuSelect.icono=data.icono;
      this.menuSelect.roles = data.roles;
      this.rolsSelect =data.roles;
      })
      this.getListRoles();
      console.log(this.menuSelect);

    }
  }
  operar() {
    if (this.menuSelect != null && this.menuSelect.idMenu > 0) {
      this.menuService.modificar(this.menuSelect).subscribe(data => {
        this.menuService.listar().subscribe(menus => {
          this.menuService.menuCambio.next(menus);
          this.menuService.mensajeCambio.next("Se modifico");
        });
      });
    } else {
      this.menuService.registrar(this.menuSelect).subscribe(data => {
        this.menuService.listar().subscribe(menus => {
          this.menuService.menuCambio.next(menus);
          this.menuService.mensajeCambio.next("Se registro");
        });
      });
    }
    this.dialogRef.close();
  }

  public getListRoles() {
    this.rolService.listar().subscribe(data => {
      this.rols = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.rolsSelect.forEach(rol => {
        this.dataSource.data.forEach(row => {
          if (row.idRol == rol.idRol) {
            this.selection.select(row)
            console.log(row)
          }
        })
        })

      console.log(data);
    })
  }
  addSelectRolToMenu() {
    this.rolsSelect = new Array;
    this.selection.selected.forEach(data => {
      console.log(data);
      this.rolsSelect.push(data);
    })
    console.log(this.rolsSelect);
  }

  saveMenu(){
    this.menuSelect.roles=this.rolsSelect;
    this.menuService.modificar(this.menuSelect).subscribe(data =>{
      this.getListRoles();
      this.menuService.mensajeCambio.next("Se Asigno roles al menu");
      this.dialogRef.close();
    })
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => {
        this.selection.select(row);
        this.rolsSelect.push(row);
      });
    console.log(this.data);
    console.log(this.rolsSelect);
  }

  masterCheckedToggle() {
    this.isAllCheckedSelected() ?
      this.checkedSelection.clear() : this.dataSource.data.forEach(row => {
        this.checkedSelection.select(row);
        this.rolsSelect.push(row);
      });
    console.log(this.rolsSelect);
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  isAllCheckedSelected() {
    const numSelected = this.checkedSelection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  remove(rol: Rol): void {
    const index = this.rolsSelect.indexOf(rol);

    if (index >= 0) {
      this.rolsSelect.splice(index, 1);
      this.dataSource.data.forEach(row => { if (row == rol) this.selection.deselect(row) });
    }
  }

  cancelar() {
    this.dialogRef.close();
  }



}
