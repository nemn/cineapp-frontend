import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { Usuario } from '../../_model/usuario';
import { environment } from '../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UsuarioService } from '../../_service/usuario.service';
import { ClienteService } from '../../_service/cliente.service';
@Component({
  selector: 'app-usuario-perfil',
  templateUrl: './usuario-perfil.component.html',
  styleUrls: ['./usuario-perfil.component.css']
})
export class UsuarioPerfilComponent implements OnInit {
  
  roles:string[];
  user:Usuario;
  username:string;
  imagenData: any;
  imagenEstado: boolean = false;
  constructor(
    private dialogRef: MatDialogRef<UsuarioPerfilComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Usuario,
    private usuarioService: UsuarioService,
    private clienteService: ClienteService,
    private sanitization: DomSanitizer
  ) { 


  }
  
  ngOnInit(): void {
    let token =sessionStorage.getItem(environment.TOKEN_NAME);
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
   console.log(decodedToken);
    this.username=decodedToken.user_name; 
    this.roles=decodedToken.authorities;
    this.usuarioService.listarPorUsuario(this.username).subscribe(data =>{
      this.user=data;
      console.log(this.user);
      this.clienteService.listarPorIdFoto(this.user.cliente.idCliente).subscribe(data =>{
        if(data.size>0)
        this.convertir(data);
      })
    })
  }
  
  convertir(data: any) {
    let reader = new FileReader();
    reader.readAsDataURL(data);
    reader.onloadend = () => {
      let base64 = reader.result;      
      this.sanar(base64);
    }
  }

  sanar(base64 : any){
    this.imagenData = this.sanitization.bypassSecurityTrustResourceUrl(base64);
    this.imagenEstado = true;
  }
  
  cancelar() {
    this.dialogRef.close();
  }
  }
