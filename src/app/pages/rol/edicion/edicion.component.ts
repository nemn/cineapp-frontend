import { Component, OnInit } from '@angular/core';
import { Pelicula } from '../../../_model/pelicula';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Rol } from '../../../_model/rol';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RolService } from '../../../_service/rol.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-edicion',
  templateUrl: './edicion.component.html',
  styleUrls: ['./edicion.component.css']
})
export class EdicionComponent implements OnInit {

  rol: Rol;
  id: number;
  edicion: boolean;
  form: FormGroup;
  idGeneroSeleccionado: number;
  urlImagen: string;
  message:string;
  constructor(
    private route: ActivatedRoute,
    private rolService: RolService,
    private router: Router) { }
  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = this.id != null;
      console.log(this.edicion);
      this.initForm();
    });


  }
  initForm() {
   if (this.edicion) {
      this.message="Modificar Rol";
      this.rolService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'idRol': new FormControl(data.idRol),
          'nombre': new FormControl(data.nombre),
          'descripcion': new FormControl(data.descripcion)
        })
      });
    } else {
      this.message="Nuevo Rol";
      this.form = new FormGroup({
        'idRol': new FormControl(0),
        'nombre': new FormControl('', [Validators.required, Validators.minLength(3)]),
        'descripcion': new FormControl('', Validators.required),
      });
    }
  }
  get f() { return this.form.controls; }
  operar() {
    if (this.form.invalid) {
      return false;
    }
    let rol = new Rol();
    rol.idRol = this.form.value['idRol'];
    rol.nombre = this.form.value['nombre'];
    rol.descripcion = this.form.value['descripcion'];
    console.log(rol);
    if (rol.idRol > 0) {
      this.rolService.modificar(rol).pipe(switchMap(() => {
        return this.rolService.listar();
      })).subscribe(data => {
        this.rolService.rolCambio.next(data);
        this.rolService.mensajeCambio.next('SE MODIFICO');

      });
    } else {
      this.rolService.registrar(rol).subscribe(() => {
        this.rolService.listar().subscribe(data => {
          this.rolService.rolCambio.next(data);
          this.rolService.mensajeCambio.next('SE REGISTRO');
        });
      })
    }
    this.router.navigate(['rol']);
  }
  operarT() {
    if (this.rol != null && this.rol.idRol > 0) {
      this.rolService.modificar(this.rol).subscribe(data => {
        this.rolService.listar().subscribe(roles => {
          this.rolService.rolCambio.next(roles);
          this.rolService.mensajeCambio.next("Se modifico");
        });
      });
    } else {
      this.rolService.registrar(this.rol).subscribe(data => {
        this.rolService.listar().subscribe(roles => {
          this.rolService.rolCambio.next(roles);
          this.rolService.mensajeCambio.next("Se registro");
        });
      });
    }
  }

}
