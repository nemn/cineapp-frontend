import { Component, OnInit, ViewChild } from '@angular/core';
import { Rol } from '../../_model/rol';
import { RolService } from '../../_service/rol.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  rol:Rol;
  dataSource: MatTableDataSource<Rol>;
  displayedColumns = ['idRol', 'nombre', 'descripcion', 'acciones'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private rolService:RolService, private dialog:MatDialog, private snackBar:MatSnackBar) { }


  ngOnInit(): void {
    this.rolService.rolCambio.subscribe(data =>{
      this.dataSource= new MatTableDataSource(data);
      this.dataSource.sort=this.sort;
      this.dataSource.paginator= this.paginator;
    });
    this.rolService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });
    this.rolService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      console.log(data);
    });
    }
    
    filter(x: string) {
      this.dataSource.filter = x.trim().toLowerCase();
    }
    eliminar(idRol : number){
      this.rolService.eliminar(idRol).pipe(switchMap( () => {
        return this.rolService.listar();
      })).subscribe(data => {
        this.rolService.rolCambio.next(data);
        this.rolService.mensajeCambio.next('SE ELIMINO');
      });
    }

  }

