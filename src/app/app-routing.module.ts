import { NuevoComponent } from './pages/login/nuevo/nuevo.component';
import { Not404Component } from './pages/not404/not404.component';
import { Not403Component } from './pages/not403/not403.component';
import { GuardService } from './_service/guard.service';
import { LoginComponent } from './pages/login/login.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
import { VentaComponent } from './pages/venta/venta.component';
import { ComidaComponent } from './pages/comida/comida.component';
import { PeliculaEdicionComponent } from './pages/pelicula/pelicula-edicion/pelicula-edicion.component';
import { PeliculaComponent } from './pages/pelicula/pelicula.component';
import { GeneroComponent } from './pages/genero/genero.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RolComponent } from './pages/rol/rol.component';
import { EdicionComponent } from './pages/rol/edicion/edicion.component';
import { MenuComponent } from './pages/menu/menu.component';
import { MenuEdicionComponent } from './pages/menu/menu-edicion/menu-edicion.component';
import { MenuAsignarComponent } from './pages/menu/menu-asignar/menu-asignar.component';
import { UsuarioRolComponent } from './pages/usuario-rol/usuario-rol.component';
import { UsuarioAsignarRolComponent } from './pages/usuario-rol/usuario-asignar-rol/usuario-asignar-rol.component';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { ClienteDialogoComponent } from './pages/cliente/cliente-dialogo/cliente-dialogo.component';
import { UsuarioPerfilComponent } from './pages/usuario-perfil/usuario-perfil.component';

const routes: Routes = [
  { path: 'genero', component: GeneroComponent, canActivate: [GuardService] },
  {
    path: 'pelicula', component: PeliculaComponent, children: [
      { path: 'nuevo', component: PeliculaEdicionComponent },
      { path: 'edicion/:id', component: PeliculaEdicionComponent }
    ], canActivate: [GuardService]
  },
  { path: 'comida', component: ComidaComponent, canActivate: [GuardService] },
  {
    path: 'rol', component: RolComponent, canActivate: [GuardService], children: [
      { path: 'nuevo', component: EdicionComponent },
      { path: 'edicion/:id', component: EdicionComponent }
    ]
  },
  {
    path: 'menu', component: MenuComponent, canActivate: [GuardService], children: [
      { path: 'nuevo', component: MenuEdicionComponent },
      { path: 'edicion/:id', component: MenuEdicionComponent },
    ]
  },
  {
    path: 'menu-rol', component: MenuComponent, canActivate: [GuardService], children: [
      { path: 'asignar/:id', component: MenuAsignarComponent },
    ]
  },
  {
    path: 'usuario-rol', component: UsuarioRolComponent, canActivate: [GuardService], children: [
      { path: 'asignar/:id', component: UsuarioAsignarRolComponent },
    ]
  },
  {
    path: 'cliente', component: ClienteComponent, canActivate: [GuardService], children: [
      { path: 'dialogo', component: ClienteDialogoComponent },
    ]
  },
  { path: 'venta', component: VentaComponent, canActivate: [GuardService] },
  { path: 'usuario-perfil', component: UsuarioPerfilComponent, canActivate: [GuardService] },
  { path: 'reporte', component: ReporteComponent, canActivate: [GuardService] },
  { path: 'consulta', component: ConsultaComponent, canActivate: [GuardService] },
  { path: 'not-403', component: Not403Component },
  { path: 'not-404', component: Not404Component },
  { path: 'login', component: LoginComponent },
  { path: 'nuevo-usuario', component: NuevoComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: 'not-404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
