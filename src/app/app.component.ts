import { MenuService } from './_service/menu.service';
import { Menu } from './_model/menu';
import { LoginService } from './_service/login.service';
import { Component, OnInit } from '@angular/core';
import { UsuarioPerfilComponent } from './pages/usuario-perfil/usuario-perfil.component';
import { MatDialog } from '@angular/material/dialog';
import { Usuario } from './_model/usuario';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  menus: Menu[];

  constructor(
    public loginService: LoginService,
    private menuService : MenuService,
    private dialog: MatDialog,
    ){  }
  
    ngOnInit(){
      this.menuService.menuCambio.subscribe(data => {
        this.menus = data;
      });
    }
    openUserPerfil() {
     let gen :Usuario = new Usuario();
      this.dialog.open(UsuarioPerfilComponent, {
        width: '400px',
        data: gen
      });
    }
}
